//Jason Le
//Functions Demo

#include <iostream>
#include <conio.h>

using namespace std;

//function prototype
void sayHello(int count = 1); //"count" here is optional //the " = 1" is the default paramater

int add(int a, int b);

bool divide(float num, float den, float &ans); //pass the answer by reference (&)

void countDownFrom(int number);

int main()
{
	//sayHello(2); //the "2" is optional

	//cout << add(3, 6);
	
	//float num1 = 0;
	//float num2 = 0;

	//cout << "Enter two numbers: ";
	//	cin >> num1;
	//	cin >> num2;

	//	float answer = 0;
	//	if (divide(num1, num2, answer))
	//	{
	//		cout << num1 << " divided by " << num2 << " is " << answer << "\n";
	//	}
	//	else 
	//	{
	//		cout << "You cannot divide by zero.\n";
	//	}

	//	cout << "num1: " << num1 << "\n";
	//	cout << "num2: " << num2 << "\n";
	char input = 'y';
	while (input == 'y')
	{
		countDownFrom(10);

		cout << "again?";
		cin >> input;
	}

	_getch();
	return 0;
}


void countDownFrom(int number)
{
	cout << number << "\n";
	if (number == 0) return;
	countDownFrom(number - 1);

}


int add(int a, int b)
{
	return a + b;
}

void sayHello(int count)
{
	for(int i = 0; i < count; i++)
	cout << "Hello!\n";
}
bool divide(float num, float den, float &ans)
{
	if (den == 0) return false;
	
	ans = num / den;

	num++;
	den += 100;

	return true;
}